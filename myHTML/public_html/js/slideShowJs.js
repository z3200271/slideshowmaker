/** 
 *
 *  @author YanLiang Li
 */

var index = 0;
var myTimeOut;
var json;
function loadJson(){
    $.getJSON("./json/my_js.json", function(data) {
        json = data;
        $("#slideshow_title").text(json.title);
        var image = document.getElementById("slideshow_image");
        image.src = ("img/" + json.slides[0].image_file_name);
        $("#slideshow_caption").text(json.slides[0].caption);
    });
}

 function nextSlide(){
    index++;
    index %= json.slides.length;
    var image = document.getElementById("slideshow_image");
    image.src = ("img/" + json.slides[index].image_file_name);
    $("#slideshow_caption").text(json.slides[index].caption);
 }

 function previousSlide(){
    var image = document.getElementById("slideshow_image");
    if(index > 0){
        index--;
        image.src = ("img/" + json.slides[index].image_file_name);
        $("#slideshow_caption").text(json.slides[index].caption);
    }
    else{
        index = json.slides.length - 1;
        image.src = ("img/" + json.slides[index].image_file_name);
        $("#slideshow_caption").text(json.slides[index].caption);
    }
 }

 function playPause(){
    var image = document.getElementById("playpausebt");
    //if click play
    if (image.src.match("Play")) {
        image.src = "icon/Pause.png";
        playFunction();
    } 
    // if click pause
    else {
        image.src = "icon/Play.png";
        clearTimeout(myTimeOut);
    }
 }

 function playFunction() {
    index++;
    index %= json.slides.length;
    var image = document.getElementById("slideshow_image");
    image.src = ("img/" + json.slides[index].image_file_name);
    $("#slideshow_caption").text(json.slides[index].caption);
    myTimeOut = setTimeout(function(){ playFunction() }, 3000);
}
