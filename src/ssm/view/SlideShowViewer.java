package ssm.view;

import java.io.File;
import java.net.URL;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.LanguagePropertyType.TOOLTIP_NEXT_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_PREVIOUS_SLIDE;
import static ssm.StartupConstants.CSS_CLASS_MY_HBOX;
import static ssm.StartupConstants.CSS_CLASS_MY_LABEL;
import static ssm.StartupConstants.CSS_CLASS_VIEW_BORDER_PANE;
import static ssm.StartupConstants.CSS_CLASS_VIEW_TOOLBAR_BUTTON;
import static ssm.StartupConstants.ICON_APPICON;
import static ssm.StartupConstants.ICON_NEXT;
import static ssm.StartupConstants.ICON_PREVIOUS;
import static ssm.StartupConstants.PATH_ICONS;
import static ssm.StartupConstants.STYLE_SHEET_UI;
import static ssm.file.SlideShowFileManager.SLASH;

/**
 *
 * @author YanLiang Li
 */
public class SlideShowViewer {
    SlideShowMakerView pastOnUi;
    Stage newStage;
    Scene newScene;
    BorderPane ssvPane;
    
    // too bar at the bottom of the view
    HBox viewToolbarPane;
    Button previousSlideBt;
    Button nextSlideBt;
    
    // main view display the picture and caption
    VBox mainView;
    ImageView imageView;
    StackPane northPane;
    Label captionLabel;
    private final String windowTitle;
    private String displayedCaption;
    private int index;
    /**
     * 
     * @param initUi 
     */
    public SlideShowViewer(SlideShowMakerView initUi){
        newStage = new Stage();
        pastOnUi = initUi;
        index = 0;
        windowTitle = pastOnUi.getSlideShow().getTitle();
        displayedCaption = "";
      
    }
    
    public String getDispalyCaption(){
        return displayedCaption;
    }
    public void setDisplayCaption(String newCap){
        displayedCaption = newCap;
    }
    /**
     * Initialize the asking language window
     * 
     */
    public void startUI(){
        initPane();
        initHandler();
        initWindow();
    }
  
    /**
     * initialize a border pane that contains a caption and a image with 
     * two button controls.
     */
    private void initPane(){
        ssvPane = new BorderPane();
        ssvPane.getStyleClass().add(CSS_CLASS_VIEW_BORDER_PANE);
        
        imageView = new ImageView();
        captionLabel = new Label();
        this.updateSlideInfo();
        
        mainView = new VBox();
      //mainView.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);
        mainView.setAlignment(Pos.CENTER);
        mainView.getChildren().add(imageView);
        ssvPane.setCenter(mainView);
        
        northPane = new StackPane();
        captionLabel.getStyleClass().add(CSS_CLASS_MY_LABEL);
        northPane.getChildren().add(captionLabel);
        northPane.setAlignment(Pos.CENTER);
        ssvPane.setTop(northPane);
        
        viewToolbarPane = new HBox();
        viewToolbarPane.setAlignment(Pos.CENTER);
        viewToolbarPane.getStyleClass().add(CSS_CLASS_MY_HBOX);
        previousSlideBt = this.initChildButton(viewToolbarPane, ICON_PREVIOUS, TOOLTIP_PREVIOUS_SLIDE, CSS_CLASS_VIEW_TOOLBAR_BUTTON);
        nextSlideBt = this.initChildButton(viewToolbarPane, ICON_NEXT, TOOLTIP_NEXT_SLIDE, CSS_CLASS_VIEW_TOOLBAR_BUTTON);
        this.updateButtons();
        ssvPane.setBottom(viewToolbarPane);
        
    }
    
    /**
     * initialize the window
     */
    private void initWindow() {
        newStage.setTitle(windowTitle);
        this.setAppIcon(ICON_APPICON);
        newScene = new Scene(ssvPane, 1000, 600);
        newScene.getStylesheets().add(STYLE_SHEET_UI);
        newStage.setScene(newScene);
        newStage.showAndWait();
    }
    
    /**
     * initialize window icon
     * @param iconFileName  name of icon in the file 
     */
    public void setAppIcon(String iconFileName){
        String imagePath = "file:" + PATH_ICONS + iconFileName;
        Image iconImage = new Image(imagePath);
        newStage.getIcons().add(iconImage);
    }
    
    public Button initChildButton(Pane toolbar,
            String iconFileName,
            LanguagePropertyType tooltip, 
            String cssClass){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.getStyleClass().add(cssClass);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
	button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        
        return button;
    }
  
    
    private void initHandler(){
        previousSlideBt.setOnAction(e -> previousSlide());
        nextSlideBt.setOnAction(e -> nextSlide());
    }
    
    /**
     * Disable/enable the two buttons accordingly
     */
    private void updateButtons(){
        previousSlideBt.setDisable(index <= 0);
        nextSlideBt.setDisable(index >= pastOnUi.getSlideShow().getSlides().size() - 1);
    }
    
    /**
     * Up date the caption and image of the slide currently displaying on the viewer
     */
    public void updateSlideInfo() {
	String imagePath = pastOnUi.getSlideShow().getSlides().get(index).getImagePath() + SLASH + 
                pastOnUi.getSlideShow().getSlides().get(index).getImageFileName();
	File file = new File(imagePath);
	try {
	    // GET AND SET THE IMAGE
	    URL fileURL = file.toURI().toURL();
	    Image slideImage = new Image(fileURL.toExternalForm());
	    imageView.setImage(slideImage);
	    
	    // AND RESIZE IT
	    //double scaledWidth = 600;
	    //double perc = scaledWidth / slideImage.getWidth();
	    //double scaledHeight = slideImage.getHeight() * perc;
	    imageView.setFitWidth(600);
	    imageView.setFitHeight(400);
            this.setDisplayCaption(pastOnUi.getSlideShow().getSlides().get(index).getCaption());
            captionLabel.setText(displayedCaption);
            
            
	} catch (Exception e) {
	    // @todo - use Error handler to respond to missing image
	}
    }    
    
    /**
     * Reload the window using updated caption and image
     */
    public void reloadViewer(){
        northPane.getChildren().clear();
        mainView.getChildren().clear();
	northPane.getChildren().add(captionLabel);
        mainView.getChildren().add(imageView);
    }
    
    private void previousSlide(){
        index--;
        this.updateButtons();
        this.updateSlideInfo();
        this.reloadViewer();
    }
    
    private void nextSlide(){
        index++;
        this.updateButtons();
        this.updateSlideInfo();
        this.reloadViewer();
    }   
}
