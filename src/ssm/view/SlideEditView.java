package ssm.view;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.StartupConstants.CSS_CLASS_MY_BUTTON;
import static ssm.StartupConstants.CSS_CLASS_MY_LABEL;
import static ssm.StartupConstants.CSS_CLASS_SELECT_SLIDE_VIEW;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_EDIT_VIEW;
import static ssm.StartupConstants.DEFAULT_THUMBNAIL_WIDTH;
import ssm.controller.ImageSelectionController;
import ssm.error.ErrorHandler;
import ssm.model.Slide;
import static ssm.file.SlideShowFileManager.SLASH;

/**
 * This UI component has the controls for editing a single slide
 * in a slide show, including controls for selected the slide image
 * and changing its caption.
 * 
 * @author McKilla Gorilla & _____YanLiangLi________
 */
public class SlideEditView extends HBox {
    // SLIDE THIS COMPONENT EDITS
    Slide slide;
    
    // DISPLAYS THE IMAGE FOR THIS SLIDE
    ImageView imageSelectionView;
    
    // CONTROLS FOR EDITING THE CAPTION
    VBox captionVBox;
    Label captionLabel;
    TextField captionTextField;
    Button saveCaptionBt;
    
    // PROVIDES RESPONSES FOR IMAGE SELECTION
    ImageSelectionController imageController;

    private ErrorHandler eH;
    
    /**
     * THis constructor initializes the full UI for this component, using
     * the initSlide data for initializing values./
     * 
     * @param initSlide The slide to be edited by this component.
     * @param ui The slide show to be edited by this component
     */
    public SlideEditView(Slide initSlide, SlideShowMakerView ui) {
	// FIRST SELECT THE CSS STYLE CLASS FOR THIS CONTAINER
	if(ui.getSlideShow().getSelectedSlide() != initSlide){
                this.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);
        }
        else{
            this.getStyleClass().add(CSS_CLASS_SELECT_SLIDE_VIEW);
        }
        eH = ui.getErrorHandler();
	
	// KEEP THE SLIDE FOR LATER
	slide = initSlide;
	
	// MAKE SURE WE ARE DISPLAYING THE PROPER IMAGE
	imageSelectionView = new ImageView();
	this.updateSlideImage();

	// SETUP THE CAPTION CONTROLS
	captionVBox = new VBox();
        captionVBox.setPrefHeight(imageSelectionView.getFitHeight());
        captionVBox.setPrefWidth(captionVBox.getPrefHeight());
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	captionLabel = new Label(props.getProperty(LanguagePropertyType.LABEL_CAPTION.toString()));
	captionTextField = new TextField();
        saveCaptionBt = new Button(props.getProperty(LanguagePropertyType.String_SAVE_CAPTION.toString()));
	captionVBox.getChildren().add(captionLabel);
	captionVBox.getChildren().add(captionTextField);
        captionVBox.getChildren().add(saveCaptionBt);
        saveCaptionBt.getStyleClass().add(CSS_CLASS_MY_BUTTON);
        captionLabel.getStyleClass().add(CSS_CLASS_MY_LABEL);

	// LAY EVERYTHING OUT INSIDE THIS COMPONENT
	getChildren().add(imageSelectionView);
	getChildren().add(captionVBox);
        captionTextField.setText(initSlide.getCaption());

	// SETUP THE EVENT HANDLERS
	imageController = new ImageSelectionController();
	imageSelectionView.setOnMousePressed(e -> {
	imageController.processSelectImage(slide, this, ui);
        ui.getFileController().markAsEdited();
	});
        saveCaptionBt.setOnAction(e -> {
        slide.setCaption(captionTextField.getText());
        ui.getFileController().markAsEdited();
        });
        captionVBox.setOnMouseClicked(e -> {
            ui.getSlideShow().setSelectedSlide(initSlide);
            ui.updateWorkSpaceControls();
            ui.reloadSlideShowPane(ui.getSlideShow());
        });
    }
    
    /**
     * This function gets the image for the slide and uses it to
     * update the image displayed.
     */
    public void updateSlideImage(){
        try {
            String imagePath = slide.getImagePath() + SLASH + slide.getImageFileName();
            File file = new File(imagePath);
            
            // GET AND SET THE IMAGE
            URL fileURL = file.toURI().toURL();
            Image slideImage = new Image(fileURL.toExternalForm());
            imageSelectionView.setImage(slideImage);
            
            // AND RESIZE IT
            double scaledWidth = DEFAULT_THUMBNAIL_WIDTH;
            double perc = scaledWidth / slideImage.getWidth();
            double scaledHeight = slideImage.getHeight() * perc;
            imageSelectionView.setFitWidth(scaledWidth);
            imageSelectionView.setFitHeight(scaledHeight);
        } catch (MalformedURLException ex) {
           eH.processError(ex.toString());
        }
    }    
}