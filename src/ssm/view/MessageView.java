
package ssm.view;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.LanguagePropertyType.String_OK;
import static ssm.LanguagePropertyType.TITLE_WINDOW;
import static ssm.StartupConstants.CSS_CLASS_BACKGROUND_COLOR;
import static ssm.StartupConstants.CSS_CLASS_MY_BUTTON;
import static ssm.StartupConstants.CSS_CLASS_MY_LABEL;
import static ssm.StartupConstants.CSS_CLASS_MY_VBOX;
import static ssm.StartupConstants.ICON_APPICON;
import static ssm.StartupConstants.PATH_ICONS;
import static ssm.StartupConstants.STYLE_SHEET_UI;

/**
 *
 * @author YanLiang Li
 */
public class MessageView {
    SlideShowMakerView pastOnUi;
    Stage newStage;
    Scene newScene;
    VBox messageVbox;
    Button okBt;
    Label messageLabel;
    
    private final String windowTitle;
    private String messageDisplay;
    
    /**
     * A window that displays a message dialog.
     * @param initUi accessing data from the main view of the application
     */
    public MessageView(SlideShowMakerView initUi) {
        newStage = new Stage();
        pastOnUi = initUi;
        windowTitle = pastOnUi.getSlideShow().getTitle();
        messageDisplay = "";
    }
    
    public String getMessageDisplay(){
        return messageDisplay;
    }
    
    public void setMessageDispaly(String message){
        messageDisplay = message;
    }
    
    public void startUi(){
        initPane();
        initHandler();
        initWindow();
    }
    
    private void initPane(){
        messageVbox = new VBox();
        messageVbox.setAlignment(Pos.CENTER);
        messageVbox.getStyleClass().add(CSS_CLASS_MY_VBOX);
        messageLabel = new Label(messageDisplay);
        messageLabel.getStyleClass().add(CSS_CLASS_MY_LABEL);
        okBt = this.initChildButton(String_OK, CSS_CLASS_MY_BUTTON);
        messageVbox.getChildren().addAll(messageLabel,okBt);
    }
    
    private void initWindow() {
        this.initTitle(TITLE_WINDOW);
        this.setAppIcon(ICON_APPICON);
        newScene = new Scene(messageVbox, 300, 100);
        messageVbox.getStyleClass().add(CSS_CLASS_BACKGROUND_COLOR);
        newScene.getStylesheets().add(STYLE_SHEET_UI);
        newStage.setScene(newScene);
        newStage.showAndWait();
    }
     
    private void setAppIcon(String iconFileName){
        String imagePath = "file:" + PATH_ICONS + iconFileName;
        Image iconImage = new Image(imagePath);
        newStage.getIcons().add(iconImage);
    }
    
    private void initTitle(LanguagePropertyType text){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String titleText = "" + props.getProperty(text.toString());
        newStage.setTitle(titleText);
    }
    
    private Button initChildButton(LanguagePropertyType text, String cssClass){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String buttonText = "" + props.getProperty(text.toString());
        Button button = new Button(buttonText);
        button.getStyleClass().add(cssClass);
        
        return button;
    }
    
    private void initHandler(){
        okBt.setOnAction(e -> newStage.close());
    }
}
