
package ssm.view;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.LanguagePropertyType.String_CHINESE;
import static ssm.LanguagePropertyType.String_ENGLISH;
import static ssm.StartupConstants.CSS_CLASS_BACKGROUND_COLOR;
import static ssm.StartupConstants.CSS_CLASS_MY_BUTTON;
import static ssm.StartupConstants.CSS_CLASS_MY_LABEL;
import static ssm.StartupConstants.ICON_APPICON;
import static ssm.StartupConstants.PATH_ICONS;
import static ssm.StartupConstants.STYLE_SHEET_UI;

/**
 * This class provides the User Interface for the user to choose a language
 * @author YanLiang Li
 */
public class LanguageSelectionView {
    // THIS IS THE MAIN APPLICATION UI WINDOW AND ITS SCENE GRAPH
    Stage newStage;
    Scene newScene;
    VBox vBox;
    Label languagePrompt;
    FlowPane fPane;
    Button ChineseSimplifiedBt;
    Button EnglishBt;
    private int languageChoosen;
    
    public LanguageSelectionView(){
        newStage = new Stage();
        languageChoosen = -1;
    }
   
    public int getLanguageChoosen(){
        return languageChoosen;
    }
    public Stage getWindow() {
	return newStage;
    }
    
    /**
     * Initialize the asking language window
     * 
     * @param windowTitle the title of the window
     */
    public void startUI(String windowTitle){
        initPane();
        initHandlers();
        initWindow(windowTitle);
    }
    
    private void initPane(){
        vBox = new VBox();
        vBox.setAlignment(Pos.CENTER);
        fPane = new FlowPane(20,20);
        fPane.setAlignment(Pos.CENTER);
        ChineseSimplifiedBt = this.initChildButton(fPane, String_CHINESE,CSS_CLASS_MY_BUTTON);
        EnglishBt = this.initChildButton(fPane, String_ENGLISH,CSS_CLASS_MY_BUTTON);
        languagePrompt = new Label("Please Select a Language");
        languagePrompt.getStyleClass().add(CSS_CLASS_MY_LABEL);
        vBox.getChildren().addAll(languagePrompt, fPane);
    }
    
    private void initWindow(String windowTitle) {
        // SET THE WINDOW TITLE
        newStage.setTitle(windowTitle);
        setAppIcon(ICON_APPICON);
        newScene = new Scene(vBox,300,100);
        newScene.getStylesheets().add(STYLE_SHEET_UI);
        newStage.setScene(newScene);
        vBox.getStyleClass().add(CSS_CLASS_BACKGROUND_COLOR);
        newStage.showAndWait();
    }
    
    private void setAppIcon(String iconFileName){
        String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image iconImage = new Image(imagePath);
        newStage.getIcons().add(iconImage);
    }
    private Button initChildButton(Pane stack, LanguagePropertyType text, String cssClass){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
      
        String buttonText = "" + props.getProperty(text.toString());
        Button button = new Button(buttonText);
        button.getStyleClass().add(cssClass);
        stack.getChildren().add(button);
        
        return button;
    }
    
    private void initHandlers(){
        ChineseSimplifiedBt.setOnAction(e -> chooseChinese());
        EnglishBt.setOnAction(e -> chooseEnglish());
    }
    
    /**
     * 
     * @return 0 if user choose Chinese Simplified
     */
    private int chooseChinese(){
        languageChoosen = 0;
        newStage.close();
        return languageChoosen;
    }
    
    /**
     * 
     * @return 1 if user choose English
     */
    private int chooseEnglish(){
        languageChoosen = 1;
        newStage.close();
        return languageChoosen;
    }
}
