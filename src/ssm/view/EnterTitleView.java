package ssm.view;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.LanguagePropertyType.DEFAULT_SLIDE_SHOW_TITLE;
import static ssm.LanguagePropertyType.LABEL_ENTER_TITLE;
import static ssm.LanguagePropertyType.String_CANCEL;
import static ssm.LanguagePropertyType.String_OK;
import static ssm.LanguagePropertyType.TITLE_WINDOW;
import static ssm.StartupConstants.CSS_CLASS_BACKGROUND_COLOR;
import static ssm.StartupConstants.CSS_CLASS_MY_BUTTON;
import static ssm.StartupConstants.CSS_CLASS_MY_LABEL;
import static ssm.StartupConstants.ICON_APPICON;
import static ssm.StartupConstants.PATH_ICONS;
import static ssm.StartupConstants.STYLE_SHEET_UI;

/**
 *
 * @author YanLiang Li
 */
public class EnterTitleView {
    Stage newStage;
    Scene newScene;
    GridPane pane;
    Button okBt;
    Button cancelBt;
    Label newLabel;
    TextField titleField;
    String slideShowTitle;
    Boolean edited;
    
    public EnterTitleView(){
        newStage = new Stage();
        edited = false;
    }
    
    public boolean isEdited(){
        return edited;
    }
    public void markTitleEdited(){
        edited = true;
    }
    public void markTitleNotEdited(){
        edited = false;
    }
    
    public String getSlideShowTitle(){
        return slideShowTitle;
    }
    
    public void setSlideShowTitle(String newTitle)
    {
        slideShowTitle = newTitle;
    }
    
    public Stage getWindow() {
        return newStage;
    }
    
    /**
     * Initialize the asking language window
     * 
     */
    public void startUI(){
        initPane();
        initHandler();
        initWindow();
    }
    
    private void initPane(){
        //set up the appearance for the window
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        pane = new GridPane();
        pane.setAlignment(Pos.CENTER);

        newLabel = new Label(props.getProperty(LABEL_ENTER_TITLE.toString()));
        newLabel.getStyleClass().add(CSS_CLASS_MY_LABEL);
        pane.add(newLabel, 0, 0);
        
        titleField = new TextField();
        titleField.setPadding(new Insets(10));
        titleField.setText(props.getProperty(DEFAULT_SLIDE_SHOW_TITLE.toString()));
        pane.add(titleField, 1, 0);
        
        okBt = new Button(props.getProperty(String_OK.toString()));
        cancelBt = new Button(props.getProperty(String_CANCEL.toString()));
        okBt.getStyleClass().add(CSS_CLASS_MY_BUTTON);
        cancelBt.getStyleClass().add(CSS_CLASS_MY_BUTTON);
        pane.add(okBt, 1, 3);
        pane.add(cancelBt, 2, 3);
        GridPane.setHalignment(okBt, HPos.RIGHT);
        GridPane.setHalignment(cancelBt, HPos.RIGHT);
        pane.getStyleClass().add(CSS_CLASS_BACKGROUND_COLOR);
    }
    
    private void initWindow() {
        this.initTitle(TITLE_WINDOW);
        setAppIcon(ICON_APPICON);
        newScene = new Scene(pane,400,100);
        newScene.getStylesheets().add(STYLE_SHEET_UI);
        newStage.setScene(newScene);
        newStage.showAndWait();
    }
    
    private void setAppIcon(String iconFileName){
        String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image iconImage = new Image(imagePath);
        newStage.getIcons().add(iconImage);
    }
    
    private void initTitle(LanguagePropertyType text){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String titleText = "" + props.getProperty(text.toString());
        newStage.setTitle(titleText);
    }
    
    private void initHandler(){
        okBt.setOnAction(e -> afterEntered());
        cancelBt.setOnAction( e -> afterCancel());
    }
    
    private void afterEntered(){
        this.setSlideShowTitle(titleField.getText());
        this.markTitleEdited();
        newStage.close();
    }
    
    private void afterCancel(){
        this.markTitleNotEdited();
        newStage.close();
    }
}
