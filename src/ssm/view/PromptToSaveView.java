package ssm.view;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.LanguagePropertyType.LABEL_PROMPT_SAVE;
import static ssm.LanguagePropertyType.String_NO;
import static ssm.LanguagePropertyType.String_YES;
import static ssm.LanguagePropertyType.TITLE_WINDOW;
import static ssm.StartupConstants.CSS_CLASS_BACKGROUND_COLOR;
import static ssm.StartupConstants.CSS_CLASS_MY_BUTTON;
import static ssm.StartupConstants.CSS_CLASS_MY_LABEL;
import static ssm.StartupConstants.ICON_APPICON;
import static ssm.StartupConstants.PATH_ICONS;
import static ssm.StartupConstants.STYLE_SHEET_UI;

/**
 *
 * @author YanLiang Li
 */
public class PromptToSaveView {
    Stage newStage;
    Scene newScene;
    BorderPane pane;
    Button saveBt;
    Button exitBt;
    Boolean chooseSave;
    
    public PromptToSaveView(){
        newStage = new Stage();
        chooseSave = false;
    }
    
    public boolean chooseToSave(){
        return chooseSave;
    }
    
    /**
     * Initialize the asking language window
     * 
     * @param windowTitle
     */
    public void startUI(){
        initPane();
        initHandler();
        initWindow();
    }
    
    private void initPane(){
        pane = new BorderPane();
        
        FlowPane northPane = new FlowPane();
        Label newLabel = this.initLabel(LABEL_PROMPT_SAVE, CSS_CLASS_MY_LABEL);
        northPane.setAlignment(Pos.CENTER);
        northPane.getChildren().add(newLabel);
        pane.setTop(northPane);
        
        HBox southPane = new HBox(25);
        southPane.setAlignment(Pos.CENTER);
        saveBt = this.initChildButton(String_YES, CSS_CLASS_MY_BUTTON);
        exitBt = this.initChildButton(String_NO, CSS_CLASS_MY_BUTTON);
        southPane.getChildren().addAll(saveBt, exitBt);
        pane.setBottom(southPane);
    }
    
    private void initWindow() {
        this.initTitle(TITLE_WINDOW);
        setAppIcon(ICON_APPICON);
        newScene = new Scene(pane, 300, 80);
        pane.getStyleClass().add(CSS_CLASS_BACKGROUND_COLOR);
        newScene.getStylesheets().add(STYLE_SHEET_UI);
        newStage.setScene(newScene);
        newStage.showAndWait();
    }
    
    private void initTitle(LanguagePropertyType text){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String titleText = "" + props.getProperty(text.toString());
        newStage.setTitle(titleText);
    }
    public void setAppIcon(String iconFileName){
        String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image iconImage = new Image(imagePath);
        newStage.getIcons().add(iconImage);
    }
    public Button initChildButton(LanguagePropertyType text, String cssClass){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String buttonText = "" + props.getProperty(text.toString());
        Button button = new Button(buttonText);
        button.getStyleClass().add(cssClass);
        
        return button;
    }
    
    public Label initLabel(LanguagePropertyType text, String cssClass){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String labelText = "" + props.getProperty(text.toString());
        Label label = new Label(labelText);
        label.getStyleClass().add(cssClass);
        
        return label;
    }
  
    
    public void initHandler(){
        saveBt.setOnAction(e -> tryToSave());
        exitBt.setOnAction(e -> notToSave());
    }
    
    private void tryToSave(){
        chooseSave = true;
        newStage.close();
    }
    
    private void notToSave(){
        chooseSave = false;
        newStage.close();
    }
}
