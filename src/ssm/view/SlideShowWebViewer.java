/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import static ssm.LanguagePropertyType.ERROR_MISSING_IMAGE;
import static ssm.LanguagePropertyType.ERROR_MISSING_JSON_FILE;
import static ssm.LanguagePropertyType.ERROR_MISSING_TEMPLET;
import static ssm.StartupConstants.ICON_APPICON;
import static ssm.StartupConstants.PATH_ICONS;
import static ssm.StartupConstants.PATH_SITES;
import static ssm.StartupConstants.PATH_SLIDE_SHOWS;
import static ssm.StartupConstants.SAMPLE_LOCATION;
import ssm.error.ErrorHandler;
import ssm.model.Slide;

/**
 *
 * @author YanLiang Li
 */
public class SlideShowWebViewer {
    SlideShowMakerView ui;
    Stage newStage;
    Scene newScene;
    final WebView browser;
    final WebEngine webEngine;
    /**
     * 
     * @param initUi 
     */
    public SlideShowWebViewer(SlideShowMakerView initUi){
        ui = initUi;
        browser = new WebView();
        webEngine = browser.getEngine();
        this.createSlideShowView();
    }
    
    public void startUi(){
        String url = "data/sites/" + ui.getSlideShow().getTitle() + "/index.html";
        File newFile = new File(url);
        try {
            webEngine.load(newFile.toURI().toURL().toString());
        } catch (MalformedURLException ex) {
            System.out.println("ERROE");
        }
        newStage = new Stage();
        newStage.setTitle(ui.getSlideShow().getTitle());
        this.setAppIcon(ICON_APPICON);
        newScene = new Scene(browser,1100,720);
        newStage.setScene(newScene);
        newStage.showAndWait();
    }
    
    /**
     * initialize window icon
     * @param iconFileName  name of icon in the file 
     */
    private void setAppIcon(String iconFileName){
        String imagePath = "file:" + PATH_ICONS + iconFileName;
        Image iconImage = new Image(imagePath);
        newStage.getIcons().add(iconImage);
    }
    
    private void createSlideShowView(){
    //create a folder inside of sites folder, and use the slideshow title as the name of the folder
        String titleFolder = PATH_SITES + ui.getSlideShow().getTitle();
        File slideShowFile = new File(titleFolder);
        slideShowFile.mkdirs();
        this.deleteDirectory(slideShowFile);
        //copy html templet to the slideshow
        try {
            this.copyDirectory(new File
                    (SAMPLE_LOCATION), slideShowFile);
        } catch (IOException e1){
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            ErrorHandler eH = ui.getErrorHandler();
            eH.processError(props.getProperty(ERROR_MISSING_TEMPLET));
        }
        
        File imgFile = new File(titleFolder + "/img");
        this.deleteDirectory(imgFile);
        // copy slideshow images to the html view folder
        for (Slide slide : ui.getSlideShow().getSlides()) {
            try {
                String fileName = slide.getImageFileName();
                File sourceFile = new File((slide.getImagePath()+slide.getImageFileName()));
                File detFile = new File(titleFolder+"/img/"+fileName);
                Files.copy(sourceFile.toPath(), detFile.toPath(), REPLACE_EXISTING);
            } catch (IOException ex) {
                PropertiesManager props = PropertiesManager.getPropertiesManager();
                ErrorHandler eH = ui.getErrorHandler();
                eH.processError(props.getProperty(ERROR_MISSING_IMAGE));
            }
	}
        
        //copy the json file into the html slide show folder
        try{
            String aTitle = ui.getSlideShow().getTitle();
            File sourceFile1 = new File(PATH_SLIDE_SHOWS+aTitle+".json");
            File detFile1 = new File (titleFolder+"/json/"+"my_js.json");
            this.deleteDirectory(new File(titleFolder+"/json"));
            Files.copy(sourceFile1.toPath(), detFile1.toPath(), REPLACE_EXISTING);
        } catch (IOException ex) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            ErrorHandler eH = ui.getErrorHandler();
            eH.processError(props.getProperty(ERROR_MISSING_JSON_FILE));
        }
    }
    //java tips
    private void copyDirectory(File sourceLocation , File targetLocation) throws IOException {
        if (sourceLocation.isDirectory()) {
            if (!targetLocation.exists()) {
                targetLocation.mkdir();
            }
             String[] children = sourceLocation.list();
             for (int i=0; i<children.length; i++) {
                 copyDirectory(new File(sourceLocation, children[i]),
                         new File(targetLocation, children[i]));
             }
         } else {   
             InputStream in = new FileInputStream(sourceLocation);
             OutputStream out = new FileOutputStream(targetLocation);
             // Copy the bits from instream to outstream
             byte[] buf = new byte[1024];
             int len;
             while ((len = in.read(buf)) > 0) {
                 out.write(buf, 0, len);
             }
             in.close();
             out.close();
         }
    }
    
    private void deleteDirectory(File directory) {
        if(directory.exists()){
            File[] files = directory.listFiles();
            if(null!=files){
                for(int i=0; i<files.length; i++) {
                    if(files[i].isDirectory()) {
                        deleteDirectory(files[i]);
                    }
                    else {
                        files[i].delete();
                    }
                }
            }
        }
    }
}
