package ssm.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import ssm.view.SlideShowMakerView;

/**
 * This class manages all the data associated with a slideshow.
 * 
 * @author McKilla Gorilla & ___YanLiangLi__________
 */
public class SlideShowModel {
    SlideShowMakerView ui;
    String title;
    ObservableList<Slide> slides;
    Slide selectedSlide;
    private int count;
    
    public SlideShowModel(SlideShowMakerView initUI) {
	ui = initUI;
	slides = FXCollections.observableArrayList();
	reset();
        count = 0;
    }

    // ACCESSOR METHODS
    public boolean isSlideSelected() {
	return selectedSlide != null;
    }
    
    public ObservableList<Slide> getSlides() {
	return slides;
    }
    
    public Slide getSelectedSlide() {
	return selectedSlide;
    }

    public String getTitle() { 
	return title; 
    }
     
    // MUTATOR METHODS
    public void setSelectedSlide(Slide initSelectedSlide) {
	selectedSlide = initSelectedSlide;
    }
    
    public void resetSelectedSlide(){
        selectedSlide = null;
    }
    
    public void setTitle(String initTitle) { 
	title = initTitle; 
    }

    public boolean isEmpty(){
        return (count == 0);
    }
    
    public boolean isMoreThanTwo(){
        return (count >= 2);
    }
   
    // SERVICE METHODS
    
    /**
     * Resets the slide show to have no slides and a default title.
     */
    public void reset() {
	slides.clear();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	title = props.getProperty(LanguagePropertyType.DEFAULT_SLIDE_SHOW_TITLE);
	selectedSlide = null;
    }

    /**
     * Adds a slide to the slide show with the parameter settings.
     * @param initImageFileName File name of the slide image to add.
     * @param initImagePath File path for the slide image to add.
     * @param initCaption caption for the slide
     */
    public void addSlide(   String initImageFileName,
			    String initImagePath, String initCaption) throws Exception {
	Slide slideToAdd = new Slide(initImageFileName, initImagePath, initCaption);
	slides.add(slideToAdd);
	//selectedSlide = slideToAdd;
	ui.reloadSlideShowPane(this);
        //MARK SLIDESHOW AS UNSAVED, AND UPDATE CONTROLS STATUS
        ui.getFileController().markAsEdited();
        count++;
    }
    
    /**
     * Removes a selected slide from the slide show.
     */
    public void removeSelectedSlide(){
        slides.remove(this.getSelectedSlide());
        this.resetSelectedSlide();
        ui.reloadSlideShowPane(this);
        ui.getFileController().markAsEdited();
        count--;
    }
    
    /**
     * Moves a selected slide up.
     */
    public void moveSlideUp() {
        int index = slides.indexOf(this.getSelectedSlide());
        Slide temp = slides.get(index - 1);
        slides.set(index - 1, this.getSelectedSlide());
        slides.set(index, temp);
        ui.reloadSlideShowPane(this);
        ui.getFileController().markAsEdited();

    }
    
    /**
     * Moves a selected slide down.
     */
    public void moveSlideDown(){  
        int index = slides.indexOf(this.getSelectedSlide());
        Slide temp = slides.get(index + 1);
        slides.set(index + 1, this.getSelectedSlide());
        slides.set(index, temp);
        ui.reloadSlideShowPane(this);
        ui.getFileController().markAsEdited();
    }
}