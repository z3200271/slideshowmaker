package ssm.controller;

import static ssm.StartupConstants.DEFAULT_SLIDE_IMAGE;
import static ssm.StartupConstants.PATH_SLIDE_SHOW_IMAGES;
import ssm.error.ErrorHandler;
import ssm.model.SlideShowModel;
import ssm.view.EnterTitleView;
import ssm.view.SlideShowMakerView;

/**
 * This controller provides responses for the slideshow edit toolbar,
 * which allows the user to add, remove, and reorder slides.
 * 
 * @author McKilla Gorilla & ____YanLiangLi_________
 */
public class SlideShowEditController {
    // APP UI
    private SlideShowMakerView ui;
    
    /**
     * This constructor keeps the UI for later.
     * @param initUI initial UI
     */
    public SlideShowEditController(SlideShowMakerView initUI) {
	ui = initUI;
    }
    
    /**
     * Provides a response for when the user wishes to add a new
     * slide to the slide show.
     */
    public void processAddSlideRequest(){
        try {
            SlideShowModel slideShow = ui.getSlideShow();
            //PropertiesManager props = PropertiesManager.getPropertiesManager();
            slideShow.addSlide(DEFAULT_SLIDE_IMAGE, PATH_SLIDE_SHOW_IMAGES, "");
            ui.getFileController().markAsEdited();
        } catch (Exception ex) {
            ErrorHandler eH = ui.getErrorHandler();
            eH.processError("11111111");
        }
    }
    
    /**
     * Provides a response for when the user wishes to remove a selected slide
     */
    public void processRemoveSlideRequest(){
        SlideShowModel slideShow = ui.getSlideShow();
        slideShow.removeSelectedSlide();
        ui.getFileController().markAsEdited();
    }
    
    /**
     * Provides a response for when the user wishes to move the selected slide 
     * down
     */
    public void processMoveSlideDownRequest(){
        SlideShowModel slideShow = ui.getSlideShow();
        slideShow.moveSlideDown();
    }
    
    /**
     * Provides a response for when the user wishes to move the selected slide
     * up
     */
    public void processMoveSlideUpRequest(){
        SlideShowModel slideShow = ui.getSlideShow();
        slideShow.moveSlideUp();
    }
    
    public void processEitTitleRequest() {
        ui.disableAllToolbarControls();
        EnterTitleView titleUi = new EnterTitleView();
        titleUi.startUI();
        if(titleUi.isEdited()){
            SlideShowModel slideShow = ui.getSlideShow();
            slideShow.setTitle(titleUi.getSlideShowTitle());
            ui.enableToolbarsControls();
            ui.getFileController().markAsEdited();
            ui.changeSaveButton(!titleUi.isEdited());
        }
        else{
            ui.changeSaveButton(!titleUi.isEdited());
            ui.enableToolbarsControls();
        }
    }
}
