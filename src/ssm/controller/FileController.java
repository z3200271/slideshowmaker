package ssm.controller;

import java.io.File;
import javafx.stage.FileChooser;
import properties_manager.PropertiesManager;
import static ssm.LanguagePropertyType.ERROR_DATA_FILE_LOADING;
import static ssm.LanguagePropertyType.LABEL_NEW_SLIDE_SHOW_MADE;
import ssm.model.SlideShowModel;
import ssm.error.ErrorHandler;
import ssm.file.SlideShowFileManager;
import ssm.view.SlideShowMakerView;
import static ssm.StartupConstants.PATH_SLIDE_SHOWS;
import ssm.view.EnterTitleView;
import ssm.view.MessageView;
import ssm.view.PromptToSaveView;
import ssm.view.SlideShowWebViewer;
/**
 * This class serves as the controller for all file toolbar operations,
 * driving the loading and saving of slide shows, among other things.
 * 
 * @author McKilla Gorilla & ______YanLiangLi_______
 */
public class FileController {

    // WE WANT TO KEEP TRACK OF WHEN SOMETHING HAS NOT BEEN SAVED
    private boolean saved;

    // THE APP UI
    private SlideShowMakerView ui;
    
    // THIS GUY KNOWS HOW TO READ AND WRITE SLIDE SHOW DATA
    private SlideShowFileManager slideShowIO;

    /**
     * This default constructor starts the program without a slide show file being
     * edited.
     *
     * @param initUI initial UI
     * @param initSlideShowIO The object that will be reading and writing slide show
     * data.
     */
    public FileController(SlideShowMakerView initUI, SlideShowFileManager initSlideShowIO) {
        // NOTHING YET
        saved = true;
	ui = initUI;
        slideShowIO = initSlideShowIO;
    }
    
   /**
    * Mark slide show as edited, and update buttons accordingly
    */
    public void markAsEdited() {
        saved = false;
        ui.updateToolbarControls(saved);
        ui.updateWorkSpaceControls();
    }
    
    
    /**
     * This method starts the process of editing a new slide show. If a pose is
     * already being edited, it will prompt the user to save it first.
     */
    public void handleNewSlideShowRequest() {
        boolean continueToMakeNew = true;
        if (!saved) {
            // THE USER CAN OPT OUT HERE WITH A CANCEL
            continueToMakeNew = promptToSave();
        }
        if (continueToMakeNew) {
            ui.disableAllToolbarControls();
            EnterTitleView titleUi = new EnterTitleView();
            titleUi.startUI();
            
            // IF USER ENTER A TITLE AND HIT OK
            // TELL THE USER THE SLIDE SHOW HAS BEEN CREATED
            if(titleUi.isEdited()){
                // RESET THE DATA, WHICH SHOULD TRIGGER A RESET OF THE UI
                SlideShowModel slideShow = ui.getSlideShow();
                // RESET THE SLIDE SHOW PANE
                slideShow.reset();
                slideShow.setTitle(titleUi.getSlideShowTitle());
                ui.reloadSlideShowPane(slideShow);
                MessageView messageView = new MessageView(ui);
                PropertiesManager props = PropertiesManager.getPropertiesManager();
                String mess = "" + props.getProperty(LABEL_NEW_SLIDE_SHOW_MADE.toString());
                messageView.setMessageDispaly(mess);
                messageView.startUi();
                saved = (!titleUi.isEdited());
                // REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
                // THE APPROPRIATE CONTROLS
                ui.updateToolbarControls(saved);
                ui.enableToolbarsControls();
                ui.updateWorkSpaceControls();
            }
            else{
                ui.enableToolbarsControls();
            }
        }
    }

    /**
     * This method lets the user open a slideshow saved to a file. It will also
     * make sure data for the current slideshow is not lost.
     */
    public void handleLoadSlideShowRequest() {
        ui.disableAllToolbarControls();
        boolean continueToOpen = true;
        if (!saved) {
            // THE USER CAN OPT OUT HERE WITH A CANCEL
            continueToOpen = promptToSave();
        }
        if (continueToOpen) {
            // GO AHEAD AND PROCEED MAKING A NEW POSE
            promptToOpen();
            ui.enableToolbarsControls();
            ui.changeSaveButton(saved);
        }
    }

    /**
     * This method will save the current slideshow to a file. Note that we already
     * know the name of the file, so we won't need to prompt the user.
     * @return true if slide is successfully saved, false otherwise
     */
    public boolean handleSaveSlideShowRequest() {
        
	    // GET THE SLIDE SHOW TO SAVE
	    SlideShowModel slideShowToSave = ui.getSlideShow();
            // SAVE IT TO A FILE
            slideShowIO.saveSlideShow(slideShowToSave, ui);
            // MARK IT AS SAVED
            saved = true;
            // AND REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
            // THE APPROPRIATE CONTROLS
            ui.updateToolbarControls(saved);
	    return true;
    }
    
    public void handlViewSlideShowRequest(){ 
        saved = true;
        this.handleSaveSlideShowRequest();
        ui.getSlideShow().resetSelectedSlide();
        ui.reloadSlideShowPane(ui.getSlideShow());
        ui.disableAllToolbarControls();
        ui.updateWorkSpaceControls();
        SlideShowWebViewer viewerUi = new SlideShowWebViewer(ui);   
        viewerUi.startUi();
        ui.enableToolbarsControls();
    }
    
     /**
     * This method will exit the application, making sure the user doesn't lose
     * any data first.
     */
    public void handleExitRequest() {
        boolean continueToExit = true;
        if (!saved) {
            // THE USER CAN OPT OUT HERE
            continueToExit = promptToSave();
        }
        if (continueToExit) {
            // EXIT THE APPLICATION
            System.exit(0);
        }
    }

    /**
     * This helper method verifies that the user really wants to save their
     * unsaved work, which they might not want to do. Note that it could be used
     * in multiple contexts before doing other actions, like creating a new
     * pose, or opening another pose, or exiting. Note that the user will be
     * presented with 3 options: YES, NO, and CANCEL. YES means the user wants
     * to save their work and continue the other action (we return true to
     * denote this), NO means don't save the work but continue with the other
     * action (true is returned), CANCEL means don't save the work and don't
     * continue with the other action (false is returned).
     *
     * @return true if the user presses the YES option to save, true if the user
     * presses the NO option to not save, false if the user presses the CANCEL
     * option to not continue.
     */
    private boolean promptToSave() {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        boolean saveWork; 
        PromptToSaveView promptSaveUi = new PromptToSaveView();
        promptSaveUi.startUI();
        saveWork = promptSaveUi.chooseToSave();

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (saveWork) {
            SlideShowModel slideShow = ui.getSlideShow();
            slideShowIO.saveSlideShow(slideShow, ui);
            saved = true;
        } // IF THE USER SAID CANCEL, THEN WE'LL TELL WHOEVER
        // CALLED THIS THAT THE USER IS NOT INTERESTED ANYMORE
        else if (!true) {
            return false;
        }

        // IF THE USER SAID NO, WE JUST GO ON WITHOUT SAVING
        // BUT FOR BOTH YES AND NO WE DO WHATEVER THE USER
        // HAD IN MIND IN THE FIRST PLACE
        return true;
    }

    /**
     * This helper method asks the user for a file to open. The user-selected
     * file is then loaded and the GUI updated. Note that if the user cancels
     * the open process, nothing is done. If an error occurs loading the file, a
     * message is displayed, but nothing changes.
     */
    private void promptToOpen() {
        // AND NOW ASK THE USER FOR THE COURSE TO OPEN
        FileChooser slideShowFileChooser = new FileChooser();
        slideShowFileChooser.setInitialDirectory(new File(PATH_SLIDE_SHOWS));
        File selectedFile = slideShowFileChooser.showOpenDialog(ui.getWindow());

        // ONLY OPEN A NEW FILE IF THE USER SAYS OK
        if (selectedFile != null) {
            try {
		SlideShowModel slideShowToLoad = ui.getSlideShow();
                slideShowIO.loadSlideShow(slideShowToLoad, selectedFile.getAbsolutePath());
                ui.reloadSlideShowPane(slideShowToLoad);
                saved = true;
                ui.updateToolbarControls(saved);
                ui.updateWorkSpaceControls();
            } catch (Exception ex) {
                PropertiesManager props = PropertiesManager.getPropertiesManager();
                String errorMessage = props.getProperty(ERROR_DATA_FILE_LOADING.toString());
                ErrorHandler eH = ui.getErrorHandler();
                eH.processError(errorMessage);
            }
        }
    }

    /**
     * This mutator method marks the file as not saved, which means that when
     * the user wants to do a file-type operation, we should prompt the user to
     * save current work first. Note that this method should be called any time
     * the pose is changed in some way.
     */
    public void markFileAsNotSaved() {
        saved = false;
    }

    /**
     * Accessor method for checking to see if the current pose has been saved
     * since it was last editing. If the current file matches the pose data,
     * we'll return true, otherwise false.
     *
     * @return true if the current pose is saved to the file, false otherwise.
     */
    public boolean isSaved() {
        return saved;
    }
}