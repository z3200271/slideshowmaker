package ssm.error;

import com.sun.javafx.tk.FontLoader;
import com.sun.javafx.tk.Toolkit;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import static ssm.StartupConstants.CSS_CLASS_BACKGROUND_COLOR;

import static ssm.StartupConstants.CSS_CLASS_MY_BUTTON;
import static ssm.StartupConstants.CSS_CLASS_MY_LABEL;
import static ssm.StartupConstants.CSS_CLASS_MY_VBOX;
import static ssm.StartupConstants.ICON_APPICON;
import static ssm.StartupConstants.PATH_ICONS;
import static ssm.StartupConstants.STYLE_SHEET_UI;
import ssm.view.SlideShowMakerView;

/**
 * This class provides error messages to the user when the occur. Note
 * that error messages should be retrieved from language-dependent XML files
 * and should have custom messages that are different depending on
 * the type of error so as to be informative concerning what went wrong.
 * 
 * @author McKilla Gorilla & ___YanLiangLi__________
 */
public class ErrorHandler {
    // APP UI
    Stage newStage;
    private SlideShowMakerView ui;
    // KEEP THE APP UI FOR LATER
    public ErrorHandler(SlideShowMakerView initUI) {
	ui = initUI;
        newStage = new Stage();
    }
    /**
     * This method provides All error feedback. It gets the feedback text,
     * which changes depending on the type of error, and presents it to
     * the user in a dialog box.
     * 
     * @param message message displayed on the dialog box
     */
    public void processError(String message)
    {
        // POP OPEN A DIALOG TO DISPLAY TO THE USER
        VBox messageVbox = new VBox();
        messageVbox.setAlignment(Pos.CENTER);
        messageVbox.getStyleClass().add(CSS_CLASS_MY_VBOX);
        Label messageLabel = new Label(message);
        messageLabel.getStyleClass().add(CSS_CLASS_MY_LABEL);
        Button okBt = new Button("OK");
        okBt.getStyleClass().add(CSS_CLASS_MY_BUTTON);
        messageVbox.getChildren().addAll(messageLabel,okBt);
        okBt.setOnAction(e -> newStage.close());
        newStage.setTitle("ERROR");
        this.setAppIcon(ICON_APPICON);
        FontLoader fontLoader = Toolkit.getToolkit().getFontLoader();
        float width = fontLoader.computeStringWidth(messageLabel.getText(), messageLabel.getFont());
        messageVbox.getStyleClass().add(CSS_CLASS_BACKGROUND_COLOR);
        Scene newScene = new Scene(messageVbox, width + 60, 100);
        newScene.getStylesheets().add(STYLE_SHEET_UI);
        newStage.setScene(newScene);
        newStage.showAndWait();
    }    
    private void setAppIcon(String iconFileName){
        String imagePath = "file:" + PATH_ICONS + iconFileName;
        Image iconImage = new Image(imagePath);
        newStage.getIcons().add(iconImage);
    }
}
